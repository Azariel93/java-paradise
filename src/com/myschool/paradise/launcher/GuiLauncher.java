package com.myschool.paradise.launcher;

import javax.swing.SwingUtilities;

import com.myschool.paradise.gui.DashBoardFrame;

public class GuiLauncher {

	public static void main(String[] args) {
		System.out.println("guiLauncher");
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new DashBoardFrame();
			}
		});
	}

}
