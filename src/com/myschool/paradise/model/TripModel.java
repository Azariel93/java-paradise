package com.myschool.paradise.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.myschool.paradise.dao.DaoFactory;
import com.myschool.paradise.dao.TripDao;

public class TripModel extends AbstractTableModel {

	private TripDao tripDao = DaoFactory.getTripDao();

	private List<Trip> trips = new ArrayList<Trip>();

	public List<Trip> getTrips() {
		return trips;
	}
	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	public Trip getTrip(int row) {
		return trips.get(row);
	}

	public TripModel(List<Trip> trips) {
		this.trips = trips;
	}

	public void refreshData() {
		trips.clear();
		trips.addAll(tripDao.findAllTrips());
	}

	public void filterData(Place startPlace, Place endPlace) {
		trips.clear();
		trips.addAll(tripDao.filter(startPlace, endPlace));
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public int getRowCount() {
		return getTrips().size();
	}

	@Override
	public Class<?> getColumnClass(int col) {
		switch (col) {
		case 0:
			return Integer.class;
		case 1 :
			return Place.class;
		case 2:
			return Place.class;
		case 3:
			return Double.class;
		default :
			return null;
	}
	}

	@Override
	public Object getValueAt(int row, int col) {
		Trip t = getTrip(row);
		switch (col) {
			case 0:
				return t.getId();
			case 1 :
				return t.getDeparture();
			case 2:
				return t.getDestination();
			case 3:
				return t.getPrice();
			default :
				return null;
		}
	}

	@Override
	public String getColumnName(int col) {
		switch (col) {
		case 0:
			return "ID";
		case 1 :
			return "Departure";
		case 2:
			return "Destination";
		case 3:
			return "Price";
		default :
			return "NONE";
		}
	}
}
