package com.myschool.paradise.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.myschool.paradise.dao.DaoFactory;
import com.myschool.paradise.dao.PlaceDao;
import com.myschool.paradise.model.Place;

@SuppressWarnings("serial")
public class AddPlaceFrame extends JFrame {

	private PlaceDao placeDao;

	public AddPlaceFrame() {

		placeDao = DaoFactory.getPlaceDao();

		setTitle("Add A Place");
		setSize(300, 200);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel mainFrame = new JPanel(new BorderLayout());

		JPanel jpanel = new JPanel(new GridBagLayout());

		GridBagConstraints g = new GridBagConstraints();

		JLabel jFrom = new JLabel("Place:");
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridx = 0;
		g.gridy = 0;
		g.insets = new Insets(0, 10, 0, 0);
		g.weightx = 1;
		jpanel.add(jFrom, g);

		JTextField jNewPlace = new JTextField();
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridwidth = 3;
		g.gridx = 1;
		g.gridy = 0;
		g.insets = new Insets(5, 0, 0, 10);
		g.weightx = 6;
		jpanel.add(jNewPlace, g);

		mainFrame.add(jpanel, BorderLayout.CENTER);

		JPanel jButtonLayout = new JPanel(new FlowLayout());

		JButton jCancel = new JButton("Cancel");
		jCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		jButtonLayout.add(jCancel);

		JButton jAddPlace = new JButton("Add Place");
		jAddPlace.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Place newPlace = new Place();

				if (jNewPlace.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Please enter a name for place");
					return;
				}

				newPlace.setName(jNewPlace.getText());
				Long id = placeDao.createPlace(newPlace);
				JOptionPane.showMessageDialog(null, "Place created successfully with id: " + id);

				setVisible(false);
			}
		});
		jButtonLayout.add(jAddPlace);

		mainFrame.add(jButtonLayout, BorderLayout.SOUTH);

		setContentPane(mainFrame);
		setVisible(true);
	}
}
