package com.myschool.paradise.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.myschool.paradise.callback.ListTripDataChange;
import com.myschool.paradise.dao.DaoFactory;
import com.myschool.paradise.dao.PlaceDao;
import com.myschool.paradise.dao.TripDao;
import com.myschool.paradise.model.Place;
import com.myschool.paradise.model.Trip;
import com.myschool.paradise.model.TripModel;

@SuppressWarnings("serial")
public class DashBoardFrame extends JFrame {

	private PlaceDao placeDao;
	private TripDao tripDao;
	List<Trip> trips = new ArrayList<Trip>();

	private Place startFilter;
	private Place endFilter;

	public DashBoardFrame() {

		placeDao = DaoFactory.getPlaceDao();
		tripDao = DaoFactory.getTripDao();
		trips = tripDao.findAllTrips();
		List<Place> places = placeDao.findAllPlaces();
		Place[] arrayPlaces = new Place[places.size()];
		places.toArray(arrayPlaces);

		setTitle("Travel Agency� Dashboard");
		setSize(600,  400);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		TripModel tModel = new TripModel(trips);
		JTable table = new JTable(tModel);

		table.setAutoCreateRowSorter(true);

		JPanel mainPanel = new JPanel();

		JPanel jPanelFilter = new JPanel(new FlowLayout());

		JComboBox jStartPlaceFilter = new JComboBox(arrayPlaces);
		jStartPlaceFilter.insertItemAt("", 0);
		jStartPlaceFilter.setSelectedIndex(0);
		jStartPlaceFilter.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (jStartPlaceFilter.getSelectedIndex() == 0) {
					startFilter = null;
					tModel.filterData(startFilter, endFilter);
					tModel.fireTableDataChanged();
				} else {
					startFilter = places.get(jStartPlaceFilter.getSelectedIndex() - 1);
					tModel.filterData(startFilter, endFilter);
					tModel.fireTableDataChanged();
				}
			}
		});

		jPanelFilter.add(jStartPlaceFilter);

		JComboBox jEndPlaceFilter = new JComboBox(arrayPlaces);
		jEndPlaceFilter.insertItemAt("", 0);
		jEndPlaceFilter.setSelectedIndex(0);
		jEndPlaceFilter.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (jEndPlaceFilter.getSelectedIndex() == 0) {
					endFilter = null;
					tModel.filterData(startFilter, endFilter);
					tModel.fireTableDataChanged();
				} else {
					endFilter = places.get(jEndPlaceFilter.getSelectedIndex() - 1);
					tModel.filterData(startFilter, endFilter);
					tModel.fireTableDataChanged();
				}
			}
		});

		jPanelFilter.add(jEndPlaceFilter);

		mainPanel.add(jPanelFilter, BorderLayout.NORTH);

		JPanel jPanelTable = new JPanel();

		JScrollPane scrollPane = new JScrollPane(table);

		Dimension d = table.getPreferredSize();

		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setPreferredSize(new Dimension(d.width, table.getRowHeight()*trips.size() + 1));

		jPanelTable.add(scrollPane);

		mainPanel.add(jPanelTable, BorderLayout.CENTER);

		JMenuBar jMenuBar = new JMenuBar();

		JMenu menu = new JMenu("Menu");

		JMenuItem jMenuHome = new JMenuItem("Home");
		jMenuHome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new DashBoardFrame();
			}
		});

		JMenuItem jMenuAddPlace = new JMenuItem("Add Place");
		jMenuAddPlace.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AddPlaceFrame();
			}
		});

		JMenuItem jMenuAddTrip = new JMenuItem("Add Trip");
		jMenuAddTrip.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AddTripFrame(new ListTripDataChange() {
					@Override
					public void onChanged() {
						tModel.refreshData();
						tModel.fireTableDataChanged();
					}
				});
			}
		});

		JMenuItem jMenuClose = new JMenuItem("Close");
		jMenuClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});

		menu.add(jMenuHome);
		menu.add(jMenuAddPlace);
		menu.add(jMenuAddTrip);
		menu.add(jMenuClose);
		jMenuBar.add(menu);

		setJMenuBar(jMenuBar);

		setContentPane(mainPanel);

		setVisible(true);
	}
}
