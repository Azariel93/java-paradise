package com.myschool.paradise.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.myschool.paradise.callback.ListTripDataChange;
import com.myschool.paradise.dao.DaoFactory;
import com.myschool.paradise.dao.PlaceDao;
import com.myschool.paradise.dao.TripDao;
import com.myschool.paradise.model.Place;
import com.myschool.paradise.model.Trip;

@SuppressWarnings("serial")
public class AddTripFrame extends JFrame {

	private PlaceDao placeDao;
	private TripDao tripDao;
	public ListTripDataChange dataChange;

	public AddTripFrame(ListTripDataChange dataChange) {

		this.dataChange = dataChange;
		placeDao = DaoFactory.getPlaceDao();
		tripDao = DaoFactory.getTripDao();

		List<Place> places = placeDao.findAllPlaces();
		Place[] arrayPlaces = new Place[places.size()];
		places.toArray(arrayPlaces);

		setTitle("Add A Trip");
		setSize(300, 200);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel mainFrame = new JPanel(new BorderLayout());

		JPanel jpanel = new JPanel(new GridBagLayout());

		GridBagConstraints g = new GridBagConstraints();
		JLabel jFrom = new JLabel("From:");
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridx = 0;
		g.gridy = 0;
		g.insets = new Insets(0, 10, 0, 0);
		g.weightx = 1;
		jpanel.add(jFrom, g);

		JComboBox jStartPlace = new JComboBox(arrayPlaces);
		jStartPlace.insertItemAt("", 0);
		jStartPlace.setSelectedIndex(0);
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridwidth = 3;
		g.gridx = 1;
		g.gridy = 0;
		g.insets = new Insets(5, 0, 0, 10);
		g.weightx = 6;
		jpanel.add(jStartPlace, g);

		JLabel jTo = new JLabel("To:");
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridx = 0;
		g.gridy = 1;
		g.insets = new Insets(0, 10, 0, 0);
		g.weightx = 1;
		jpanel.add(jTo, g);

		JComboBox jEndPlace = new JComboBox(arrayPlaces);
		jEndPlace.insertItemAt("", 0);
		jEndPlace.setSelectedIndex(0);
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridwidth = 3;
		g.gridx = 1;
		g.gridy = 1;
		g.insets = new Insets(5, 0, 0, 10);
		g.weightx = 6;
		jpanel.add(jEndPlace, g);

		JLabel jPrice = new JLabel("Price:");
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridx = 0;
		g.gridy = 2;
		g.insets = new Insets(0, 10, 0, 0);
		g.weightx = 1;
		jpanel.add(jPrice, g);

		JTextField jPriceTrip = new JTextField();
		g.fill = GridBagConstraints.HORIZONTAL;
		g.gridwidth = 1;
		g.gridx = 1;
		g.gridy = 2;
		g.insets = new Insets(5, 0, 0, 10);
		g.weightx = 1;
		jpanel.add(jPriceTrip, g);

		mainFrame.add(jpanel, BorderLayout.CENTER);

		JPanel jButtonLayout = new JPanel(new FlowLayout());

		JButton jCancel = new JButton("Cancel");
		jCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		jButtonLayout.add(jCancel);

		JButton jAddTrip = new JButton("Add Trip");
		jAddTrip.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Trip trip = new Trip();

				if (jStartPlace.getSelectedIndex() == 0) {
					JOptionPane.showMessageDialog(null, "Please choose a valid place for departure");
					return;
				}

				Place departure = new Place();
				departure.setId(places.get(jStartPlace.getSelectedIndex() - 1).getId());
				trip.setDeparture(departure);

				if (jEndPlace.getSelectedIndex() == 0) {
					JOptionPane.showMessageDialog(null, "Please choose a valid place for destination");
					return;
				}

				Place destination = new Place();
				destination.setId(places.get(jEndPlace.getSelectedIndex() - 1).getId());
				trip.setDestination(destination);

				if (Pattern.matches("^[0-9]+(\\.[0-9]+)?$", jPriceTrip.getText())) {
					trip.setPrice(Double.parseDouble(jPriceTrip.getText()));
					Long id = tripDao.createTrip(trip);

					JOptionPane.showMessageDialog(null, "Trip created successfully with id: " + id);

					dataChange.onChanged();

					setVisible(false);
				} else {
					JOptionPane.showMessageDialog(null, "Please enter a valid number for price");
					return;
				}
			}
		});
		jButtonLayout.add(jAddTrip);

		mainFrame.add(jButtonLayout, BorderLayout.SOUTH);

		setContentPane(mainFrame);
		setVisible(true);
	}
}
