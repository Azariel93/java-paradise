package com.myschool.paradise.callback;

public interface ListTripDataChange {

	void onChanged();

}
